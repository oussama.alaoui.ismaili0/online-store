package emsi.intense

import javax.validation.constraints.DecimalMin

class BootStrap {

    def init = { servletContext ->

        def rand = new Random()



        def userAdmin = new User(username: 'admin', password: 'admin').save()
        def userMod = new User(username: 'modo', password: 'modo').save()
        def userClient = new User(username: 'client',password: 'client').save()

        def roleAdmin = new Role(authority: 'ROLE_ADMIN').save()
        def roleMod = new Role(authority: 'ROLE_MODO').save()
        def roleClient = new Role(authority: 'ROLE_CLIENT').save()

        UserRole.create(userAdmin, roleAdmin, true)
        UserRole.create(userMod, roleMod, true)
        UserRole.create(userClient,roleClient,true)

        ["Alice","Bob","Charly","David","Etienne"].each {
            String name ->
                def userInstance = new User(username: name, password: "password").save()
                UserRole.create(userInstance,roleClient,true)
                (1..5).each {
                    Integer annonceIdx ->
                        def random = Math.abs(rand.nextInt() % 10 )+1
                        def Desc
                        def Name
                        def Price
                        switch (random)
                        {
                            case 1:
                                Name = "Apple iPhone 12 (64GB) - Black"
                                Desc = "6.1-inch Super Retina XDR Display\n" +
                                        "Ceramic Shield. Stronger than any smartphone glass\n" +
                                        "Ready for 5G\n" +
                                        "A14 Bionic chip, the fastest chip ever in a smartphone\n" +
                                        "12MP dual camera system: ultra wide angle and wide angle cameras, 2x optical zoom out"
                                Price = 700+random*30
                                break
                            case 2:
                            //ipad mini
                            Name = "2021 Apple iPad Mini (Wi-Fi, 64GB) - Pink"
                            Desc = "8.3-inch Liquid Retina display with True Tone and wide color\n" +
                                    "A15 Bionic chip with Neural Engine\n" +
                                    "Touch ID for secure authentication and Apple Pay\n" +
                                    "12MP Wide back camera, 12MP Ultra Wide front camera with Center Stage\n" +
                                    "Available in purple, starlight, pink, and space gray\n" +
                                    "Landscape stereo speakers\n" +
                                    "Stay connected with ultrafast Wi-Fi 6\n" +
                                    "Up to 10 hours of battery life\n" +
                                    "USB-C connector for charging and accessories\n" +
                                    "Works with Apple Pencil (2nd generation)"
                            Price = 800+random*30
                            break
                            case 3:
                            //white t-shirt
                            Name = "Minimalistic white T-Shirt"
                            Desc="Solids: 100% Cotton; Ash Grey: 99% Cotton, 1% Polyester; Sport Grey and Antique Heathers: 90% Cotton, 10% Polyester; Safety Colors and Heathers: 50% Cotton, 50% Polyester\n" +
                                    "Imported\n" +
                                    "Pull On closure\n" +
                                    "Machine Wash\n" +
                                    "Thick and heavy 6.0 oz. per sq. yd. fabric made from 100% US cotton\n" +
                                    "Featuring taped neck and shoulders, and double-needle collar, sleeves and hem for long-lasting comfort and durability\n" +
                                    "Pull on closure for grab-and-go wearability\n" +
                                    "Classic fit offering plenty of move-around, breathable room\n" +
                                    "Perfect for everyday wear - lounging-around or hanging-out\n" +
                                    "Available in multiple pack sizes and a variety of colors"
                            Price = 30+random*12
                            break
                            case 4:
                            //sneakers
                            Name = "Mens Running Shoes Sneakers"
                            Desc = "Fabric upper with good air permeability\n" +
                                    "Rubber sole\n" +
                                    "Fashion Stylish Sneakers: Look lively and full of personality with the exceptional style of the fashion sneaker. Simple and stylish flower scheme, looking for beauty in simple life.\n" +
                                    "Light-Weight & Breathable Shoes: Fashion mesh upper for ultra-lightweight support and breathability, keeps the feet dry and breathable, makes you fell comfortable while exercising, allowing to move the way they were meant to, to enjoy the unparalleled pleasure of free walking jogging and running, bring new feeling to you.\n" +
                                    "Non-Slip Walking Shoes: The whole outsole is made of non slip rubber+PU sole which will provide you with much support, and the anti-skid ripple on the bottom can increase traction and provides impact cushioning, anti-twist, abrasion-resistant, and anti-slip perform with the ground, high elasticity and shock absorption to provide security for walking and running. These are perfect sports gym shoes for men and women.\n" +
                                    "Unique Designer Sports Shoes: The rubbe with high-density cushioning outsole protects the feet from sharp but also reducing the risk of your ankles' sprain. And the fashion and unique design, it's perfect choice to pair with any occasion: casual, walking, running, training, indoor, sports, outdoor, travel, exercise, workout and so on. It's very easy to choose the right clothes to match.\n"
                            Price = 100+random*20
                            break
                            case 5:
                            //ps5
                            Name ="PS5™ Digital Edition Covers – Nova Pink"
                            Desc = "Choose Your Color. Covers are for the PS5 Digital Edition console.\n" +
                                    "Explore gaming frontiers with new covers for your PS5 Digital Edition. Pair them with a matching DualSense™ wireless controller to complete the look.**"
                            Price = 400+random*15
                            break
                            case 6:
                            //macbook pro
                            Name = "Apple MacBook Pro with 2.0GHz Intel Core i5 (13-inch, 16GB RAM, 512GB SSD Storage) - Silver"
                            Desc = "Series\tMWP72LL/A\n" +
                                    "Screen Size\t13 Inches\n" +
                                    "Color\tSilver\n" +
                                    "Hard Disk Size\t512 GB\n" +
                                    "CPU Model\tCore i5-1035G7\n" +
                                    "Ram Memory Installed Size\t16 GB\n" +
                                    "Operating System\tMac OS\n" +
                                    "Special Feature\tIPS Retina Display, P3 Color Gamut | True Tone Technology, Touch Bar | Touch ID Sensor, Magic Keyboard | Force Touch TrackpadIPS Retina Display, P3 Color Gamut | True Tone Technology, Touch Bar | Touch ID Sensor, Magic Keyboard | Force Touch Trackpad\n" +
                                    "Card Description\tIntegrated"
                            Price = 800+random*25
                            break
                            case 7:
                            //standing desk
                            Name= "Electric Standing Desk, 55 x 28 inches Height Adjustable Desk"
                            Desc  = "The Electric height adjustable standing desk has 4 preset buttons to customize your desired heights from 27.75\" to 46.06\" , can be used by people of different heights in different scenes."
                            Price =90+random*25
                            break
                            case 8:
                            //thread mill
                            Name="Fitness TR300 Folding Treadmill"
                            Desc="Large 20\" x 55\" walking/running surface accommodates users of many sizes and stride lengths\n" +
                                    "Intuitive 5.5\" Blue Backlit LCD display is easy to read and keeps you updated on speed, incline, time, distance, calories and pulse\n" +
                                    "24 preset programs, heart rate control, and manual program offer unmatched variety for your workouts\n" +
                                    "Lift Assist and Safe Drop folding deck technology makes setup and storage safe and easy\n" +
                                    "Handlebar mounted speed and incline controls provide you with a secondary location to change your workout intensity - they are right where you need them for a comfortable and secure workout"
                            Price=300+random*15
                            break
                            case 9:
                                //backpack
                                Name="Travel Laptop Backpack"
                                Desc = "★LOTS OF STORAGE SPACE&POCKETS: One separate laptop compartment hold 15.6 Inch Laptop as well as 15 Inch,14 Inch and 13 Inch Laptop. One spacious packing compartment roomy for daily necessities,tech electronics accessories. Front compartment with many pockets, pen pockets and key fob hook, makes your item organized and easier to find\n" +
                                        "★COMFY&STURDY: Comfortable soft padded back design with thick but soft multi-panel ventilated padding, gives you maximum back support. Breathable and adjustable shoulder straps relieve the stress of shoulder. Foam padded top handle for a long time carry on\n" +
                                        "★FUNCTIONAL&SAFE: A luggage strap allows backpack fit on luggage/suitcase, slide over the luggage upright handle tube for easier carrying. With a hidden anti theft pocket on the back protect your valuable items from thieves. Well made for international airplane travel and day trip as a travel gift for men\n" +
                                        "★USB PORT DESIGN: With built in USB charger outside and built in charging cable inside,this usb backpack offers you a more convenient way to charge your phone while walking. It's a great tech gift for him from wife, daughter and son. Please noted that this backpack doesn't power itself, usb charging port only offers an easy access to charge\n" +
                                        "★DURABLE MATERIAL&SOLID: Made of Water Resistant and Durable Polyester Fabric with metal zippers. Ensure a secure & long-lasting usage everyday & weekend.Serve you well as professional office work bag,slim USB charging bagpack,college high school big students backpacks for boys,girls,teens"
                                Price = 5+random*10
                            break
                            case 10:
                            Name="Apple Pencil (2nd Generation)"
                            Desc = "Compatible with iPad mini (6th generation), iPad Air (5th and 4th generation), iPad Pro 12.9-inch (3rd, 4th, and 5th generations), iPad Pro 11-inch (3rd, 2nd, and 1st generations)\n" +
                                    "Apple Pencil (2nd generation) brings your work to life. With imperceptible lag, pixel-perfect precision, and tilt and pressure sensitivity, it transforms into your favorite creative instrument, your paint brush, your charcoal, or your pencil.\n" +
                                    "It makes painting, sketching, doodling, and even note-taking better than ever.\n" +
                                    "It magnetically attaches to iPad mini (6th generation), iPad Pro and iPad Air, charges wirelessly, and lets you change tools with a simple double tap.\n"
                            Price = 100+random*20
                            break
                            //apple pencil
                        }

                        def annonceInstance =
                                new Annonce(
                                        title: "Annonce $Name par $name",
                                        description: "$Desc",
                                        price: Price,
                                        status: Boolean.TRUE,
                                )
                        (1..3).each {
                            annonceInstance.addToIllustrations(new Illustration(filename: "item$random-$it-.png"))
                        }
                        userInstance.addToAnnonces(annonceInstance)
                }
                userInstance.save(flush: true, failOnError: true)
        }

    }
    def destroy = {
    }
}
