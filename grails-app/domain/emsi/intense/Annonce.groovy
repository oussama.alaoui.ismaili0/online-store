package emsi.intense

class Annonce {

    String title
    String description
    Float price
    Boolean status = Boolean.FALSE
    Date dateCreated
    Date lastUpdated
    List illustrations

    static belongsTo = [author: User]

    static hasMany = [illustrations: Illustration]

    static constraints = {
        title nullable: false, blank: false, size: 6..255
        description nullable: true, blank: true, minSize: 10
        price nullable: false, min: 0F
        status nullable: false
    }

    static mapping = {
        description type: 'text'
    }
}
