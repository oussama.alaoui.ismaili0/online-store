<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

    <title>Welcome to </title>
</head>
<body>



    <content tag="nav">
%{--<sec:ifAnyGranted roles="ROLE_ADMIN ,ROLE_MODO, ROLE_CLIENT">--}%
%{--        <a>Welcome You are logged in <sec:username />  </a>--}%
%{--</sec:ifAnyGranted>--}%
        <li class="dropdown">
<sec:ifNotLoggedIn>
        <a href="/login/index">Login</a>
</sec:ifNotLoggedIn>
              </li>
        <li>
<sec:ifAnyGranted roles="ROLE_ADMIN ,ROLE_MODO,ROLE_CLIENT ">

            <a href="/logout/index">Logout</a>

</sec:ifAnyGranted>
        </li>

    </content>

    <div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="online_shoping.png" class="grails-logo"/>
        </div>
    </div>

    <div id="content" role="main">

        <section class="row colset-2-its">
            <h1 id="main-title">Welcome to ShopFromHome <sec:username /></h1>
            <div id="controllers" role="navigation">


%{--                    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.name } }">--}%
                        <div class="controller">
                                 <div class="controller-button">
                                     <a href="/annonce/index" style="color: white; text-decoration: none; font-size: 30px" >Annonces </a>

%{--                                     <g:link controller="${c.logicalPropertyName}"> ${c.name}</g:link>--}%
                                 </div>

            </div>

%{--                <div class="controller">--}%
%{--                <div class="controller-button">--}%

%{--                    <a href="/illustration/index" style="color: white; text-decoration: none; font-size: 30px" >illustrations </a>--}%
%{--                </div>--}%
%{--                </div>--}%
                <div class="controller">
                <div class="controller-button">

                    <a href="/User/index" style="color: white; text-decoration: none; font-size: 30px" >Users </a>
                </div>
                </div>
%{--                    </g:each>--}%

            </div>
        </section>
    </div>

</body>
</html>
