<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
<sec:ifAnyGranted roles="ROLE_ADMIN ,ROLE_MODO">
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
</sec:ifAnyGranted>
            </ul>
        </div>

        <div id="list-annonce" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <h2 id="annonce-sub">NOTE: You can click on any item to see more options and details</h2>

            <div class="annonces-grid ">

            %{--            <f:table collection="${annonceList}" />--}%
                <g:each in="${annonceList}" var="c" >

                    <a href="/annonce/show/${c.id}" style="text-decoration: none;color: black">   <div class="annonce-box">

                        <div class="image-grid">  <g:each in="${c.illustrations}" var="i" >  <asset:image src="${i.filename}" class="ill-images"/></g:each> </div>
                        <div style="font-size: 20px;text-align: center; font-weight: bold";>${c.title}</div>
                        <div  style="font-size: 16px;text-align: center;   overflow: hidden;
                        text-overflow: ellipsis;
                        display: -webkit-box;
                        -webkit-line-clamp: 5; /* number of lines to show */
                        line-clamp: 5;
                        -webkit-box-orient: vertical;">${c.description}</div>
                        <div style="font-size: 20px;text-align: center;font-weight: bold;color: #00a044">${c.price} $</div>
                    </div></a>
                </g:each>

            </div>

            <div class="pagination">
                <g:paginate total="${annonceCount ?: 0}" />
            </div>
        </div>
    </body>
</html>