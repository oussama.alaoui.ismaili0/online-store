package emsi.intense

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_CLIENT','ROLE_ADMIN'])

class ApiController {

    /**
     * GET / PUT / PATCH / DELETE
     */
    AnnonceService annonceService
    UserService userService

    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = 404
                renderThis(annonceInstance, request.getHeader('Accept'))
                break
            case "PUT":
                if (!params.id)
                    return response.status = 400
                def annonceInstance = Annonce.get(params.id)
                if(params.description && params.title && params.price && params.author)
                {
                    def author = User.get(params.author);
                    if(author){
                        if(params.status=="true")
                        {
                            annonceInstance.author=author;
                            annonceInstance.title=params.title;
                            annonceInstance.description=params.description;
                            annonceInstance.price=Float.valueOf( params.price);
                            annonceInstance.status=true;
                            renderThis(annonceInstance, request.getHeader('Accept'))
                        }
                        if(params.status=="false")
                        {
                            annonceInstance.author=author;
                            annonceInstance.title=params.title;
                            annonceInstance.description=params.description;
                            annonceInstance.price=Float.valueOf(params.price);
                            annonceInstance.status=false;

                        }
                        annonceService.save(annonceInstance)
                        renderThis(annonceInstance, request.getHeader('Accept'))
                    }
                }
                //add image from body and convert it
                return response.status = 400

                break
            case "PATCH":
                if (!params.id)
                    return response.status = 400
                def annonceInstance = Annonce.get(params.id)
                if(params.description)
                annonceInstance.description=params.description;
                if(params.status=="true")
                    annonceInstance.status=true;
                if(params.status=="false")
                    annonceInstance.status=false;
                if(params.title)
                    annonceInstance.title=params.title;
                if(params.price)
                    annonceInstance.price= Float.valueOf(params.price);
                if(params.author)
                {
                    def author = User.get(params.author);
                    if(author)
                    annonceInstance.author=author;
                }
                //add image from body and convert it

                annonceService.save(annonceInstance)
                renderThis(annonceInstance, request.getHeader('Accept'))

                break
            case "DELETE":
                if (!params.id)
                {
                    return response.status = 400
                }
                Annonce.get(params.id).delete(flush:true);

                return response.status = 200
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406
    }

    def annonces() {
    if(request.getMethod()=="GET")
    {
        def annonceList = Annonce.all;
        if(!annonceList)
        {
            return response.status = 404
        }
        renderThis(annonceList, request.getHeader('Accept'))
    }
        else
    {
        return response.status=405
    }
        return response.status = 406
    }

    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400

                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                renderThis(userInstance, request.getHeader('Accept'))
                break
            case "PUT":

                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                if(params.username && params.password && params.enabled && params.accountExpired && params.accountLocked  && params.passwordExpired )
                {
                    userInstance.username = params.username
                    userInstance.password = params.password
                    if(params.enabled =="true")
                        userInstance.enabled = true
                    if(params.enabled == "false")
                        userInstance.enabled = false
                    if(params.accountExpired == "true")
                        userInstance.accountExpired = true
                    if(params.accountExpired == "false")
                        userInstance.accountExpired = false
                    if(params.accountLocked== "true")
                        userInstance.accountLocked = true
                    if(params.accountLocked== "false")
                        userInstance.accountLocked = false
                    if(params.passwordExpired== "true")
                        userInstance.passwordExpired = true
                    if(params.passwordExpired== "false")
                        userInstance.passwordExpired = false

                    userService.save(userInstance)
                    renderThis(userInstance, request.getHeader('Accept'))

                }
                return response.status = 400

                break
            case "PATCH":
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                if(params.username)
                    userInstance.username = params.username
                if(params.password)
                    userInstance.password = params.password
                if(params.enabled =="true")
                    userInstance.enabled = true
                if(params.enabled == "false")
                    userInstance.enabled = false
                if(params.accountExpired == "true")
                    userInstance.accountExpired = true
                if(params.accountExpired == "false")
                    userInstance.accountExpired = false
                if(params.accountLocked== "true")
                    userInstance.accountLocked = true
                if(params.accountLocked== "false")
                    userInstance.accountLocked = false
                if(params.passwordExpired== "true")
                    userInstance.passwordExpired = true
                if(params.passwordExpired== "false")
                    userInstance.passwordExpired = false

                userService.save(userInstance)
                renderThis(userInstance, request.getHeader('Accept'))
                break
            case "DELETE":
                if (!params.id)
                {
                    return response.status = 400
                }
                User.get(params.id).delete(flush:true);
                return response.status = 200
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406
    }

    def users() {
        if(request.getMethod()=="GET")
        {
            def usersList = User.all;
            if(!usersList)
            {
                return response.status = 404
            }
            renderThis(usersList, request.getHeader('Accept'))
        }
        else
        {
            return response.status=405
        }
        return response.status = 406
    }

    def renderThis(Object instance, String accept )
    {
        switch (accept)
        {
            case "json":
            case "application/json":
            case "text/json":
                render instance as JSON
                break;
            case "xml":
            case "application/xml":
            case "text/xml":
                render instance as XML
                break;
        }
    }
}
