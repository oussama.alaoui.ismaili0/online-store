# online-store

Ce projet est une plateforme de mise en ligne d'annonces dans le cadre d'un cours sur Groovy on Rails ressemblant à https://www.leboncoin.fr/.

Il y a 3 Types d'utilisateurs avec des droits d'accès distincts
  - Clients
      +Ils ont accès à l'API
      +Il ne peuvent que voir les annonces existantes
  - Modérateurs
      +Ils peuvent effectuer des opérations CRUD sur les annonces et illustrations
  - Admins
      +Ils ont tout les droits possibles

L'application est divisée en 2 parties:
Une partie API : 
  - Elle implémente les requetes Get (byId et All) / Post / Patch / Put / Delete pour les entités Annonces et Users.
  - Un système d'autentification à l'aide de Token permet de protèger l'API à partir du header
    Authorization qui contient "Bearer" suivi d'un token qui peut être obtenu à partir de /api/login.
  - Les requêtes peuvent retourner du Json ou du XML à partir du header Accept.
  - Le code des erreurs revoyées est représentatif de l'état de la requête.

Une partie web permettant de visualisezr les annonces et leurs illustrations et respectant les drotis d'accès mentionnés précédemment.

Des données générées aléatoirement ont été ajoutées à l'application pour un meilleur réalisme et une meilleure expérience utilisateur.

L'application est hebergée sur Heroku et accessible à partir du lien suivant :
